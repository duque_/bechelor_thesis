/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2018 STMicroelectronics International N.V.
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "usb_device.h"
#include "usbd_cdc_if.h"

//#define FLASH_KEY1      ((uint32_t)0x45670123)
//#define FLASH_KEY2      ((uint32_t)0xCDEF89AB)
#define flash_page        0x0801FC00
#define FLASH_DATA_SIZE   28

SPI_HandleTypeDef hspi1;

extern int setting_flag;
int flag = 0;
extern uint8_t setting[11];

void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_SPI1_Init(void);

typedef struct CSx_s {
	GPIO_TypeDef *PORT;
    uint16_t PIN;
 } CSx_t;

CSx_t CS_1[16] = {{  GPIOB, GPIO_PIN_0  },{  GPIOC, GPIO_PIN_4  },{  GPIOA, GPIO_PIN_2  },{  GPIOA, GPIO_PIN_0  },{  GPIOC, GPIO_PIN_2  },{  GPIOC, GPIO_PIN_0  },{  GPIOE, GPIO_PIN_4  },{  GPIOE, GPIO_PIN_2  },
									{  GPIOE, GPIO_PIN_0  },{  GPIOB, GPIO_PIN_8  },{  GPIOD, GPIO_PIN_7  },{  GPIOD, GPIO_PIN_5  },{  GPIOD, GPIO_PIN_3  },{  GPIOD, GPIO_PIN_1  },{  GPIOC, GPIO_PIN_11 },{  GPIOD, GPIO_PIN_11 }};

CSx_t CS_2[16] = {{  GPIOC, GPIO_PIN_5  },{  GPIOA, GPIO_PIN_3  },{  GPIOA, GPIO_PIN_1  },{  GPIOC, GPIO_PIN_3  },{  GPIOC, GPIO_PIN_1  },{  GPIOC, GPIO_PIN_15 },{  GPIOE, GPIO_PIN_3  },{  GPIOE, GPIO_PIN_1  },
									{  GPIOB, GPIO_PIN_9  },{  GPIOB, GPIO_PIN_7  },{  GPIOD, GPIO_PIN_6  },{  GPIOD, GPIO_PIN_4  },{  GPIOD, GPIO_PIN_2  },{  GPIOD, GPIO_PIN_0  },{  GPIOC, GPIO_PIN_10 },{  GPIOD, GPIO_PIN_10 }};

CSx_t HGO[16]  = {{  GPIOA, GPIO_PIN_9  },{  GPIOA, GPIO_PIN_10 },{  GPIOC, GPIO_PIN_9  },{  GPIOA, GPIO_PIN_8  },{  GPIOC, GPIO_PIN_14 },{  GPIOE, GPIO_PIN_5  },{  GPIOE, GPIO_PIN_6  },{  GPIOC, GPIO_PIN_13 },
								  {  GPIOB, GPIO_PIN_5  },{  GPIOB, GPIO_PIN_6  },{  GPIOD, GPIO_PIN_15 },{  GPIOC, GPIO_PIN_8  },{  GPIOD, GPIO_PIN_14 },{  GPIOC, GPIO_PIN_12 },{  GPIOD, GPIO_PIN_12 },{  GPIOD, GPIO_PIN_13 }};


uint16_t conf[16];
uint16_t hgo[4];
uint16_t R1[4];
uint16_t R2[4];

uint8_t check_setting[16];


void write_flash(uint16_t* data, uint8_t size)
{
	uint16_t i;

	FLASH->KEYR = FLASH_KEY1;
	FLASH->KEYR = FLASH_KEY2;  			 // Unblock writing

	while(FLASH_SR_BSY&FLASH->SR);   // Check if flash is not busy

	FLASH->CR |= FLASH_CR_PER; 			 // Page Erase Set
	FLASH->AR = flash_page; 			   // Page Address
	FLASH->CR |= FLASH_CR_STRT; 		 // Start Page Erase

	while(FLASH_SR_BSY&FLASH->SR);   // Check if flash is not busy

	FLASH->CR &= ~FLASH_CR_PER; 		 // Page Erase Clear
	FLASH->CR |= FLASH_CR_PG;				 // Enable flash writing

	for(i=0; i<size; i++)						 // Write 16bit data to flash memory
	{
		while(FLASH_SR_BSY&FLASH->SR);

		*(__IO uint16_t*)(flash_page + i*2) = data[i];
	}

	FLASH->CR &= ~FLASH_CR_PG;				// Disable flash writing
	FLASH->CR |= FLASH_CR_LOCK;				// Block writing
}


void read_flash(uint16_t* data, uint8_t size)
{
	uint16_t i;
	for(i=0; i<size; i++)
	{
		data[i] = *(__IO uint16_t*)(flash_page + i*2);
	}

}


uint8_t set_pot_A(CSx_t CSx)
{
	uint8_t err = 0;
	uint8_t answer[2] = {0,0};
	uint8_t msg[2] = {0,0};

	HAL_GPIO_WritePin(CSx.PORT, CSx.PIN, GPIO_PIN_RESET);

	msg[0] = 0x00;
	msg[1] = R1[0];
	HAL_SPI_TransmitReceive(&hspi1, msg, answer, 2, 1000);
	check_setting[0] = answer[0];
	check_setting[1] = answer[1];

	msg[0] = 0x01;
	msg[1] = R1[1];
	HAL_SPI_TransmitReceive(&hspi1, msg, answer, 2, 1000);
	check_setting[2] = answer[0];
	check_setting[3] = answer[1];

	msg[0] = 0x10;
	msg[1] = R1[2];
	HAL_SPI_TransmitReceive(&hspi1, msg, answer, 2, 1000);
	check_setting[4] = answer[0];
	check_setting[5] = answer[1];

	msg[0] = 0x11;
	msg[1] = R1[3];
	HAL_SPI_TransmitReceive(&hspi1, msg, answer, 2, 1000);
	check_setting[6] = answer[0];
	check_setting[7] = answer[1];

	HAL_GPIO_WritePin(CSx.PORT, CSx.PIN, GPIO_PIN_SET);

	return err;
}


uint8_t set_pot_B(CSx_t CSx)
{
	uint8_t err = 0;
	uint8_t answer[2] = {0,0};
	uint8_t msg[2] = {0,0};

	HAL_GPIO_WritePin(CSx.PORT, CSx.PIN, GPIO_PIN_RESET);

	msg[0] = 0x00;
	msg[1] = R2[0];
	HAL_SPI_TransmitReceive(&hspi1, msg, answer, 2, 1000);
	check_setting[8] = answer[0];
	check_setting[9] = answer[1];

	msg[0] = 0x01;
	msg[1] = R2[1];
	HAL_SPI_TransmitReceive(&hspi1, msg, answer, 2, 1000);
	check_setting[10] = answer[0];
	check_setting[11] = answer[1];

	msg[0] = 0x10;
	msg[1] = R2[2];
	HAL_SPI_TransmitReceive(&hspi1, msg, answer, 2, 1000);
	check_setting[12] = answer[0];
	check_setting[13] = answer[1];

	msg[0] = 0x11;
	msg[1] = R2[3];
	HAL_SPI_TransmitReceive(&hspi1, msg, answer, 2, 1000);
	check_setting[14] = answer[0];
	check_setting[15] = answer[1];

	HAL_GPIO_WritePin(CSx.PORT, CSx.PIN, GPIO_PIN_SET);

	return err;
}

uint8_t set_pot()
{
	uint8_t i = 0;
	uint8_t err = 0;
	for(i = 0; i <= 15; i++)
	{
		if(conf[i])
		{
			if(set_pot_A(CS_1[i]) != 0) err = -1;
			if(set_pot_B(CS_2[i]) != 0) err = -1;
		}
	}

	return err;
}


int main(void)
{

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* Configure the system clock */
  SystemClock_Config();

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USB_DEVICE_Init();
  MX_SPI1_Init();

  uint8_t ret = 0;
  uint16_t data[FLASH_DATA_SIZE];

  read_flash(data, FLASH_DATA_SIZE);

  memcpy(conf,      data, 16);
  memcpy(R1,   data + 16,  4);
	memcpy(R2,   data + 20,  4);
	memcpy(hgo,  data + 24,  4);

	ret = set_pot();

  /* Infinite loop */
  uint8_t i = 0;

  while (1)
  {
  	if (setting_flag == 1)
  	{
  		uint8_t x = 0b10000000;
			for (i = 0; i <= 7; i++)
			{
				conf[i] = (setting[0] & x) >> (7 - i);
				x = x >> 1;
			}

			x = 0b10000000;
			for (i = 8; i <= 15; i++)
			{
				conf[i] = (setting[1] & x) >> (15 - i);
				x = x >> 1;
			}

			x = 0b11000000;
			for (i = 0; i <= 3; i++)
			{
				R1[i] = setting[i + 2];
				R2[i] = setting[i + 6];
				hgo[i] = (setting[10] & x) >> (6 - 2*i);
								x = x >> 2;
			}


			memcpy(data,      conf, 16);
			memcpy(data + 16,   R1,  4);
			memcpy(data + 16,   R2,  4);
			memcpy(data + 24,  hgo,  4);

			write_flash(data, FLASH_DATA_SIZE);

			ret = set_pot();

					if(ret == 0)
					{
						ret = 0;
					}

  		setting_flag = 0;
  		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, GPIO_PIN_SET);
  	}

  }
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /**Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /**Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USB;
  PeriphClkInit.UsbClockSelection = RCC_USBCLKSOURCE_PLL_DIV1_5;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief SPI1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI1_Init(void)
{
  /* SPI1 parameter configuration*/
  hspi1.Instance = SPI1;
  hspi1.Init.Mode = SPI_MODE_MASTER;
  hspi1.Init.Direction = SPI_DIRECTION_2LINES;
  hspi1.Init.DataSize = SPI_DATASIZE_16BIT;
  hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_8;
  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi1.Init.CRCPolynomial = 10;
  if (HAL_SPI_Init(&hspi1) != HAL_OK)
  {
    Error_Handler();
  }

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
	 GPIO_InitTypeDef GPIO_InitStruct = {0};

	/* GPIO Ports Clock Enable */
	__HAL_RCC_GPIOA_CLK_ENABLE();
	__HAL_RCC_GPIOB_CLK_ENABLE();
	__HAL_RCC_GPIOC_CLK_ENABLE();
	__HAL_RCC_GPIOD_CLK_ENABLE();
	__HAL_RCC_GPIOE_CLK_ENABLE();


	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0|GPIO_PIN_7|GPIO_PIN_8|GPIO_PIN_9, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3|GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_10|GPIO_PIN_11|GPIO_PIN_15, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3|GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_6|GPIO_PIN_7|GPIO_PIN_10|GPIO_PIN_11, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOE, GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3|GPIO_PIN_4, GPIO_PIN_RESET);

	CSx_t HGO[16]  = {{  GPIOA,   },{  GPIOA,  },{  GPIOC,   },{  GPIOA,   },{  GPIOC,  },{  GPIOE,  },{  GPIOE,   },{  GPIOC,  },
									  {  GPIOB,   },{  GPIOB,   },{  GPIOD,  },{  GPIOC,   },{  GPIOD,  },{  GPIOC,  },{  GPIOD,  },{  GPIOD,  }};


	/*Configure GPIO pin : PA11 */ //todo
	GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3|GPIO_PIN_8|GPIO_PIN_9|GPIO_PIN_10;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	/*Configure GPIO pin : PB15 */
	GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_5|GPIO_PIN_6|GPIO_PIN_7|GPIO_PIN_8|GPIO_PIN_9;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	/*Configure GPIO pins : PE2 PE3 PE4 */
	GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3|GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_8|GPIO_PIN_9|GPIO_PIN_10|GPIO_PIN_11|GPIO_PIN_12|GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_15;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

	/*Configure GPIO pin : PD2 */
	GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3|GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_6|GPIO_PIN_7|GPIO_PIN_10|GPIO_PIN_11|GPIO_PIN_12|GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_15;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

	/*Configure GPIO pins : PE2 PE3 PE4 */
	GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3|GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_6;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
