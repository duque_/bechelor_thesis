import serial 
import sys
from math import sqrt, pi, pow

ser_alive = 1

serial_port = "insert serial port"

filter = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
colon = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
comma = [-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
comma_R = [-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
comma_index = 1
comma_index_R = 1
comma_index_H = 1
colon_index = 0
setting = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
comma_H = [-1, 0, 0, 0, 0, 0, 0]
H = [0,0,0,0]
R1 = [0, 0, 0, 0]
R2 = [0, 0, 0, 0]
is_200k = 1
error = 0
is_set = 0
error_H = 0
print

for i in range(0,len(sys.argv)):
    # help section 
    if (sys.argv[i] == "--help"):
        print("-p    [set serial port]")
        print("-V    [set values - 8 numbers in range 0-255, comma separated]")
        print("       First four for R1 values, last four for R2 values, as follows:")
        print("       1,5: first stage")
        print("       2,6: second stage")
        print("       3,7: third stage")
        print("       4,8: fourth stage")
        print("-h    [set gain for each stage [0 - 0dB, 1 - 10dB] e.g 0,0,1,0 (default: 0dB for each stage)]")
        print("-n    [set filters, comma separated, use colon for setting in range")
        print("       e.g. 1,3,5:11,16]")

    # set port
    if (sys.argv[i] == "-p" and i != len(sys.argv)-1):
        serial_port = sys.argv[i+1]

    # set resistors values
    if (sys.argv[i] == "-V" and i != len(sys.argv)-1):
        is_set = 1
        for j in range (0, len(sys.argv[i+1])):
            if(sys.argv[i+1][j] == ","):
                comma_R[comma_index_R] = j
                comma_index_R = comma_index_R + 1
        if(comma_index_R == 8):
            for ii in range(0, 4):
                R1[ii] = int(sys.argv[i+1][comma_R[ii]+1:comma_R[ii+1]])
            for ii in range(4, 7):
                R2[ii-4] = int(sys.argv[i+1][comma_R[ii]+1:comma_R[ii+1]])
            R2[3] = int(sys.argv[i+1][comma_R[ii+1]+1:len(sys.argv[i+1])])
        else:
           print("____________________________________________")
           print("Resistors values inserted incorrectly")
           error = 1

    # set gain of each stage
    if (sys.argv[i] == "-h" and i != len(sys.argv)-1):
        for j in range (0, len(sys.argv[i+1])):
            if(sys.argv[i+1][j] == ","):
                comma_H[comma_index_H] = j
                comma_index_H = comma_index_H + 1
        print("xDDD")
        print(comma_index_H)
        if(comma_index_H == 4):
            for ii in range(0, 3):
                value = int(sys.argv[i+1][comma_H[ii]+1:comma_H[ii+1]])
                if(value == 0 or value == 1):
                    H[ii] = value
                else:
                    error_H = 1
            value = int(sys.argv[i+1][comma_H[ii+1]+1:len(sys.argv[i+1])])
            if(value == 0 or value == 1):
                H[3] = value
            else:
                error_H = 1
            if(error_H == 1):
                print("Gain insertet incorrectly, set to default: [0,0,0,0]")
                H = [0,0,0,0]
        else:
           print("Gain insertet incorrectly, set to default: [0,0,0,0]")
           H = [0,0,0,0]

    # set for which filters configuration has to be set up
    if (sys.argv[i] == "-n" and i != len(sys.argv)-1):
        filter = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        for j in range (0, len(sys.argv[i+1])):
            if(sys.argv[i+1][j] == ":"):
                colon[colon_index] = j
                colon_index = colon_index + 1
            if(sys.argv[i+1][j] == ","):
                comma[comma_index] = j
                comma_index = comma_index + 1
        if(comma_index == 1):
            is_colon = 0
            for x in range(0,len(sys.argv[i+1])):
                print(sys.argv[i+1][x])
                if(sys.argv[i+1][x] == ":"):
                    is_colon = 1
                    first  = int(sys.argv[i+1][0:x])-1
                    second = int(sys.argv[i+1][x+1:len(sys.argv[i+1])])-1
                    for s in range (first,second+1):
                        if(s < 17 and s > 0):
                            filter[s] = 1
            if(is_colon == 0):
                if(int(sys.argv[i+1]) < 17 and int(sys.argv[i+1]) > 0):
                    filter[int(sys.argv[i+1])-1] = 1 
        else:
            for ii in range(0, comma_index-1):
                setting[ii] = sys.argv[i+1][comma[ii]+1:comma[ii+1]]
            setting[ii+1] = sys.argv[i+1][comma[ii+1]+1:len(sys.argv[i+1])]
            for jj in range(0,ii+2):
                is_colon = 0
                for x in range(0,len(setting[jj])):
                    if(setting[jj][x] == ":"):
                        is_colon = 1
                        first  = int(setting[jj][0:x])
                        second = int(setting[jj][x+1:len(setting[jj])])
                        for s in range (first,second+1):
                            if(s < 17 and s > 0):
                                filter[s-1] = 1
                if(is_colon == 0):
                    if(int(setting[jj]) < 17 and int(setting[jj]) > 0):
                        filter[int(setting[jj])-1] = 1 

# print setting information
if(is_set == 0):
    print("____________________________________________")
    print("Resistor values not defined, use -V flag to set")
  
print("____________________________________________")
print
print("Filters to set:")
print(filter)
print
print("Gain:")
print(H)
print

if(error == 0):
    try:
        ser = serial.Serial(serial_port) 
    except serial.serialutil.SerialException:
        print("connection fault, use -p flag to define serial port")
        ser_alive = 0

    # variables init
    C1     = [5.6e-09,  6.8e-09,  10e-09,   22e-09]
    C2     = [4.7e-09,  3.9e-09, 2.7e-09, 0.82e-09]
    Q      = [ 0.5098,   0.6013,     0.9,   2.5629]
    freq   = [0, 0, 0, 0]
    w0     = [0, 0, 0, 0]
    f      = [0, 0, 0, 0]
    a      = [0, 0, 0, 0]
    J      = [0, 0, 0, 0]
    q      = 784.314
    R1d = [0, 0, 0, 0]
    R2d = [0, 0, 0, 0]

    print("____________________________________________")

    
    print
    if(is_200k):
        #for 200k resistors
        MAX = 200000
        print("200k")
    else:
        #for 20k resistors
        MAX = 20000
        q = q/10    
        print("20k")

    # calculate information about filters with actual setting
    for i in range (0,4):
        R1d[i] = int(R1[i] / q) + int((R1[i] % q)/q + 0.5)
        R2d[i] = int(R2[i] / q) + int((R2[i] % q)/q + 0.5)

        if(R1d[i] > 255): R1d[i] = 255
        if(R2d[i] > 255): R2d[i] = 255

        R1[i] = R1d[i] * q
        R2[i] = R2d[i] * q

        w0[i] = 1 / (sqrt(R1[i] * R2[i] * C1[i] * C2[i]))
        f[i] = w0[i] / (2 * pi)
        a[i] = (R1[i] + R2[i]) / (2 * C1[i] * R1[i] * R2[i])
        Q[i] = w0[i] / (2*a[i])

    # print information
    print("%8.0f %8.0f %8.0f %8.0f" % (f[0], f[1], f[2], f[3]))
    print
    print(R1d)
    print(R2d)
    print
    print("R1 200k: %8.0f %8.0f %8.0f %8.0f" % (R1[0], R1[1], R1[2], R1[3]))
    print("R2 200k: %8.0f %8.0f %8.0f %8.0f" % (R2[0], R2[1], R2[2], R2[3]))
    print(" Q 200k: %8.2f %8.2f %8.2f %8.2f" % (Q[0], Q[1], Q[2], Q[3]))

    print("____________________________________________")
      
    # create binary message to be sent
    filter_bin1 = 0
    for i in range (0,8):
        filter_bin1 = (filter_bin1 << 1) | filter[i]

    filter_bin2 = 0
    for i in range (9,16):
        filter_bin2 = (filter_bin2 << 1) | filter[i]

    hgo = 0x00
    for i in range (0,4):
        hgo = (hgo << 2) | H[i]

    packet = bytearray()
    packet.append(filter_bin1)
    packet.append(filter_bin2)
    packet.append(R1d[0])
    packet.append(R1d[1])
    packet.append(R1d[2])
    packet.append(R1d[3])
    packet.append(R2d[0])
    packet.append(R2d[1])
    packet.append(R2d[2])
    packet.append(R2d[3])
    packet.append(hgo)

    # send the message and wait for receiving message back
    if (ser_alive):
        print("Configuration sent.")
        ser.write(msg)
        line = ser.read(11)
        print(line.encode('hex'))
