import serial 
import sys
from math import sqrt, pi, pow

ser_alive = 1

serial_port = "insert serial port"

filter = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
colon = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
comma = [-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
comma_index = 1
colon_index = 0
setting = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

is_200k = 1

print

for i in range(0,len(sys.argv)):
    if (sys.argv[i] == "--help"):
        print("-f    [set frequency]")
        print("-p    [set serial port]")
        print("-v    [set 200k or 20k (default: 200k)]")
        print("-n    [set filters, comma separated, use colon for setting in range")
        print("       e.g. 1,3,5:11,16]")
    if (sys.argv[i] == "-f" and i != len(sys.argv)-1):
        input_freq = int(sys.argv[i+1])
    if (sys.argv[i] == "-p" and i != len(sys.argv)-1):
        serial_port = sys.argv[i+1]
    if (sys.argv[i] == "-v" and i != len(sys.argv)-1):
        if(sys.argv[i+1] == "20k"): 
            is_200k = 0
        elif(sys.argv[i+1] == "200k"):
            is_200k = 1
        else:
            print("wrong version input, set to default: 200k")
            print
    if (sys.argv[i] == "-n" and i != len(sys.argv)-1):
        filter = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        for j in range (0, len(sys.argv[i+1])):
            if(sys.argv[i+1][j] == ":"):
                colon[colon_index] = j
                colon_index = colon_index + 1
            if(sys.argv[i+1][j] == ","):
                comma[comma_index] = j
                comma_index = comma_index + 1
        if(comma_index == 1):
            is_colon = 0
            for x in range(0,len(sys.argv[i+1])):
                print(sys.argv[i+1][x])
                if(sys.argv[i+1][x] == ":"):
                    is_colon = 1
                    first  = int(sys.argv[i+1][0:x])-1
                    second = int(sys.argv[i+1][x+1:len(sys.argv[i+1])])-1
                    for s in range (first,second+1):
                        if(s < 17 and s > 0):
                            filter[s] = 1
            if(is_colon == 0):
                if(int(sys.argv[i+1]) < 17 and int(sys.argv[i+1]) > 0):
                    filter[int(sys.argv[i+1])-1] = 1 
        else:
            for ii in range(0, comma_index-1):
                setting[ii] = sys.argv[i+1][comma[ii]+1:comma[ii+1]]
            setting[ii+1] = sys.argv[i+1][comma[ii+1]+1:len(sys.argv[i+1])]
            for jj in range(0,ii+2):
                is_colon = 0
                for x in range(0,len(setting[jj])):
                    if(setting[jj][x] == ":"):
                        is_colon = 1
                        first  = int(setting[jj][0:x])
                        second = int(setting[jj][x+1:len(setting[jj])])
                        for s in range (first,second+1):
                            if(s < 17 and s > 0):
                                filter[s-1] = 1
                if(is_colon == 0):
                    if(int(setting[jj]) < 17 and int(setting[jj]) > 0):
                        filter[int(setting[jj])-1] = 1 
print         
print(filter)
print

try:
    ser = serial.Serial(serial_port) 
except serial.serialutil.SerialException:
    print("connection fault, use -p flag to define serial port")
    ser_alive = 0

C1     = [5.6e-09,  6.8e-09,  10e-09,   22e-09]
C2     = [4.7e-09,  3.9e-09, 2.7e-09, 0.82e-09]
Q      = [ 0.5098,   0.6013,     0.9,   2.5629]
freq   = [0, 0, 0, 0]
R1     = [0, 0, 0, 0]
R2     = [0, 0, 0, 0]
w0     = [0, 0, 0, 0]
f      = [0, 0, 0, 0]
a      = [0, 0, 0, 0]
J      = [0, 0, 0, 0]
q      = 784.314
R1d = [0, 0, 0, 0]
R2d = [0, 0, 0, 0]

print("____________________________________________")

if (isinstance(input_freq, str) or input_freq < 0):
    print("\nInsert frequency\n");
else:
    print
    if(is_200k):
        #for 200k resistors
        MAX = 200000
        print("200k")
    else:
        #for 20k resistors
        MAX = 20000
        q = q/10
        print("20k")

    flag = 0
    for i in range(0,4):
        freq[i] = input_freq
        while(flag == 0):
            R1[i] = (1 + sqrt(1 - 4*Q[i]*Q[i] * (C2[i]/C1[i]) )) / (4 * pi * freq[i] * C2[i] * Q[i]); #Ohm
            R2[i] = 1 / (2  *pi * freq[i] * C2[i]*Q[i] ) - R1[i]; #Ohm
            if (R1[i] > MAX or R2[i] > MAX):
                freq[i] = freq[i] + 0.1
            else:
                flag = 1
        flag = 0

        R1d[i] = int(R1[i] / q) + int((R1[i] % q)/q + 0.5)
        R2d[i] = int(R2[i] / q) + int((R2[i] % q)/q + 0.5)

        R1[i] = R1d[i] * q
        R2[i] = R2d[i] * q

        w0[i] = 1 / (sqrt(R1[i] * R2[i] * C1[i] * C2[i]))
        f[i] = w0[i] / (2 * pi)
        a[i] = (R1[i] + R2[i]) / (2 * C1[i] * R1[i] * R2[i])
        Q[i] = w0[i] / (2*a[i])

        J[i] = pow(((input_freq - f[i]) / input_freq), 2) + 2 * pow(((Q[i] - Q[i]) / Q[i]), 2)
       
    
    print("%8.0f %8.0f %8.0f %8.0f" % (f[0], f[1], f[2], f[3]))
    print(R1d)
    print(R2d)
    print("R1 200k: %8.0f %8.0f %8.0f %8.0f" % (R1[0], R1[1], R1[2], R1[3]))
    print("R2 200k: %8.0f %8.0f %8.0f %8.0f" % (R2[0], R2[1], R2[2], R2[3]))
    print(" Q 200k: %8.2f %8.2f %8.2f %8.2f" % (Q[0], Q[1], Q[2], Q[3]))
    print(" J 200k: %8.2f %8.2f %8.2f %8.2f" % (J[0], J[1], J[2], J[3]))

    print("____________________________________________")
      
    print(str(hex(R1d[0]) + " " + hex(R1d[1]) + " " +  hex(R1d[2]) + " " +  hex(R1d[3]) + " " +  hex(R2d[0]) + " " +  hex(R2d[1]) + " " +  hex(R2d[2]) + " " +  hex(R2d[3])))
    print




    msg = str(hex(R1d[0]) + hex(R1d[1]) + hex(R1d[2]) + hex(R1d[3]) + hex(R2d[0]) + hex(R2d[1]) + hex(R2d[2]) + hex(R2d[3]))
    print(msg)

    if (ser_alive):
        ser.write(msg)
        line = ser.read(8)
        print(line.encode('hex'))
