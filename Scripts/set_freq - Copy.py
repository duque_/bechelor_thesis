import serial 
import sys
from math import sqrt, pi, pow

ser1_200k_alive = 1
ser2_200k_alive = 1
ser1_20k_alive = 1
ser2_20k_alive = 1
ser2_20k_alive = 1

try:
    ser1_200k = serial.Serial("/dev/ttyACM0")
except serial.serialutil.SerialException:
    print("ser1_200k not connected")
    ser1_200k_alive = 0

try:
    ser2_200k = serial.Serial('/dev/ttyUSB0') 
except serial.serialutil.SerialException:
    print("ser2_200k not connected")
    ser2_200k_alive = 0

try:
    ser1_20k = serial.Serial('/dev/ttyUSB0') 
except serial.serialutil.SerialException:
    print(" ser1_20k not connected")
    ser1_20k_alive = 0

try:
    ser2_20k = serial.Serial('/dev/ttyUSB0') 
except serial.serialutil.SerialException:
    print(" ser2_20k not connected")
    ser2_20k_alive = 0

try:
    ser3_20k = serial.Serial('/dev/ttyUSB0') 
except serial.serialutil.SerialException:
    print(" ser3_20k not connected")
    ser3_20k_alive = 0


C1     = [5.6e-09,  6.8e-09,  10e-09,   22e-09]
C2     = [4.7e-09,  3.9e-09, 2.7e-09, 0.82e-09]
Q      = [ 0.5098,   0.6013,     0.9,   2.5629]
Q_200k = [ 0.5098,   0.6013,     0.9,   2.5629]
Q_20k  = [ 0.5098,   0.6013,     0.9,   2.5629]
freq   = [0, 0, 0, 0]
R1     = [0, 0, 0, 0]
R2     = [0, 0, 0, 0]
w0     = [0, 0, 0, 0]
f      = [0, 0, 0, 0]
a      = [0, 0, 0, 0]
J      = [0, 0, 0, 0]
q_200k = 784.314
q_20k  = 78.4314
R1d_200k = [0, 0, 0, 0]
R2d_200k = [0, 0, 0, 0]
R1d_20k  = [0, 0, 0, 0]
R2d_20k  = [0, 0, 0, 0]

if len(sys.argv) > 1:
    input_freq = int(sys.argv[1])
else:
    input_freq = -1


if (isinstance(input_freq, str) or input_freq < 0):
    print("\nInsert frequency\n");
else:
    #for 200k resistors
    flag = 0
    for i in range(0,4):
        freq[i] = input_freq
        while(flag == 0):
            R1[i] = (1 + sqrt(1 - 4*Q[i]*Q[i] * (C2[i]/C1[i]) )) / (4 * pi * freq[i] * C2[i] * Q[i]); #Ohm
            R2[i] = 1 / (2  *pi * freq[i] * C2[i]*Q[i] ) - R1[i]; #Ohm
            if (R1[i] > 200000 or R2[i] > 200000):
                freq[i] = freq[i] + 0.1
            else:
                flag = 1
        flag = 0

        R1d_200k[i] = int(R1[i] / q_200k) + int((R1[i] % q_200k)/q_200k + 0.5)
        R2d_200k[i] = int(R2[i] / q_200k) + int((R2[i] % q_200k)/q_200k + 0.5)

        R1[i] = R1d_200k[i] * q_200k
        R2[i] = R2d_200k[i] * q_200k

        w0[i] = 1 / (sqrt(R1[i] * R2[i] * C1[i] * C2[i]))
        f[i] = w0[i] / (2 * pi)
        a[i] = (R1[i] + R2[i]) / (2 * C1[i] * R1[i] * R2[i])
        Q_200k[i] = w0[i] / (2*a[i])

        J[i] = pow(((input_freq - f[i]) / input_freq), 2) + 2 * pow(((Q[i] - Q_200k[i]) / Q[i]), 2)
       
    print
    print("freq 200k: %8.0f %8.0f %8.0f %8.0f" % (f[0], f[1], f[2], f[3]))
    print(R1d_200k)
    print(R2d_200k)
    print("R1 200k: %8.0f %8.0f %8.0f %8.0f" % (R1[0], R1[1], R1[2], R1[3]))
    print("R2 200k: %8.0f %8.0f %8.0f %8.0f" % (R2[0], R2[1], R2[2], R2[3]))
    print(" Q 200k: %8.2f %8.2f %8.2f %8.2f" % (Q_200k[0], Q_200k[1], Q_200k[2], Q_200k[3]))
    print(" J 200k: %8.2f %8.2f %8.2f %8.2f" % (J[0], J[1], J[2], J[3]))

    #for 20k resistors
    flag = 0
    for i in range(0,4):
        freq[i] = input_freq
        while(flag == 0):
            R1[i] = (1 + sqrt(1 - 4*Q[i]*Q[i] * (C2[i]/C1[i]) )) / (4 * pi * freq[i] * C2[i] * Q[i]); #Ohm
            R2[i] = 1 / (2  *pi * freq[i] * C2[i]*Q[i] ) - R1[i]; #Ohm
            if (R1[i] > 20000 or R2[i] > 20000):
                freq[i] = freq[i] + 0.1
            else:
                flag = 1
        flag = 0
        
        R1d_20k[i] = int(R1[i] / q_20k) + int((R1[i] % q_20k)/q_20k + 0.5)
        R2d_20k[i] = int(R2[i] / q_20k) + int((R2[i] % q_20k)/q_20k + 0.5)

        R1[i] = R1d_20k[i] * q_20k
        R2[i] = R2d_20k[i] * q_20k

        w0[i] = 1 / (sqrt(R1[i] * R2[i] * C1[i] * C2[i]))
        f[i] = w0[i] / (2 * pi)
        a[i] = (R1[i] + R2[i]) / (2 * C1[i] * R1[i] * R2[i])
        Q_20k[i] = w0[i] / (2*a[i])

        J[i] = pow(((input_freq - f[i]) / input_freq), 2) + 2 * pow(((Q[i] - Q_200k[i]) / Q[i]), 2)
        
    print
    print("freq 20k: %8.0f %8.0f %8.0f %8.0f" % (f[0], f[1], f[2], f[3]))
    print(R1d_20k)
    print(R2d_20k)
    print("R1  20k: %8.0f %8.0f %8.0f %8.0f" % (R1[0], R1[1], R1[2], R1[3]))
    print("R2  20k: %8.0f %8.0f %8.0f %8.0f" % (R2[0], R2[1], R2[2], R2[3]))
    print(" Q  20k: %8.2f %8.2f %8.2f %8.2f" % (Q_20k[0], Q_20k[1], Q_20k[2], Q_20k[3]))
    print(" J  20k: %8.2f %8.2f %8.2f %8.2f" % (J[0], J[1], J[2], J[3]))

    msg_200k = str(hex(R1d_200k[0]) + hex(R1d_200k[1]) + hex(R1d_200k[2]) + hex(R1d_200k[3]) + hex(R2d_200k[0]) + hex(R2d_200k[1]) + hex(R2d_200k[2]) + hex(R2d_200k[3]))
    msg_20k  = str(hex(R1d_20k[0])  + hex(R1d_20k[1])  + hex(R1d_20k[2])  + hex(R1d_20k[3] ) + hex(R2d_20k[0])  + hex(R2d_20k[1])  + hex(R2d_20k[2])  + hex(R2d_20k[3] ))
    print(msg_200k)
    print(msg_20k)

    if (ser1_200k_alive):
        ser1_200k.write(msg_200k)
        line = ser1_200k.read(8)
        print(line.encode('hex'))

    if (ser2_200k_alive):
        ser1_200k.write(msg_200k)
        line = ser2_200k.read(8)
        print(line.encode('hex'))

    if (ser1_00k_alive):
        ser1_200k.write(msg_20k)
        line = ser1_20k.read(8)
        print(line.encode('hex'))

    if (ser2_20k_alive):
        ser1_200k.write(msg_20k)
        line = ser2_20k.read(8)
        print(line.encode('hex'))

    if (ser3_20k_alive):
        ser1_200k.write(msg_20k)
        line = ser3_20k.read(8)
        print(line.encode('hex'))
