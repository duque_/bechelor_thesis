format longE
s = tf('s');

%%INSERT CUTOFF FREQUENCY_______________________________________________
freq = 1000;

%%CALCULATE ELEMENTS VALUES_____________________________________________
C1 = [ 5.6 * 10^(-9),           6.8 * 10^(-9),           10 * 10^(-9),   22 * 10^(-9)   ]; %F
C2 = [ 4.7 * 10^(-9),           3.9 * 10^(-9),           2.7 * 10^(-9),  0.82 * 10^(-9) ]; %F
Q =  [ (1/2)*sqrt(C1(1)/C2(1)), (1/2)*sqrt(C1(2)/C2(2)), 1/sqrt(2),      1/sqrt(2)      ];

flag = 0
for i = 1:4
    while flag == 0
    R1(i) = (1 + sqrt(1 - 4*Q(i).^2*(C2(i)/C1(i))))/(4*pi*freq*C2(i)*Q(i)); %Ohm
    R2(i) = 1/(2*pi*freq*C2(i)*Q(i)) - R1(i);                               %Ohm
        if R1(i) > 20000 | R2(i) > 20000 
             Q(i) = Q(i) + 0.01;
        else
            flag = 1;
        end
    end
    flag = 0;
end

%%TRANSFER FUNCTION COEFFICIENTS
for i = 1:4 
    w0(i) = 1 / (sqrt(R1(i)*R2(i)*C1(i)*C2(i)));
    f(i) = w0(i)/(2*pi);
    a(i) = (R1(i) + R2(i))/(2*C1(i)*R1(i)*R2(i));
    Q(i) = w0(i)/(2*a(i));
    K(i) = w0(i)^2 / (s^2 + 2*a(i)*s + w0(i)^2);
end

%%DRAW FREQUENCY CHARACTERISTICS________________________________________
vec = 0:100:1000000;
figure 
for i = 1:4
    subplot(2,2,i);
    bode(K(i), vec);
    grid on
    titl = strcat({'Filter nr '}, int2str(i), ':');
    title(titl)
end

%%SHOW RESULTS__________________________________________________________
frequency = sprintf('%.0f  ', f),  Q_factor = sprintf('%.6f   ', Q),  Resistance_1 = sprintf('%.3fkOhm   ', R1/1000), Resistance_2 = sprintf('%.3fkOhm   ', R2/1000)


%%Z?o?enie filtr�w
G = K(1) * K(2) * K(3) * K(4)
figure
subplot(1,2,1)
bode(K(1), vec)
grid on
title('One filter')
subplot(1,2,2)
bode(G, vec);
grid on
title('All filters')

